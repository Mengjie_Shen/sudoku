/**
 * Created by mshe666 on 16/11/2017.
 */
public class Sudoku {
    private int[][] game = new int[9][9];
    private final int[] numLine = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    public void start() {
/*        for (int i = 0; i < 9; i++) {
            int[] newArray = numLine;
            for (int j = 0; j < 9; j++) {
                int[] rowArray = rowToArray(game,i);
                int[] colArray = columnToArray(game,j);
                int[] blkArray = blockToArray(game,i,j);
                while (newArray.length > 0) {
                    int cell = pickANum(newArray);
                    if (isValidInArray(cell,rowArray) && isValidInArray(cell,colArray) && isValidInArray(cell,blkArray)) {
                        game[i][j] = cell;
                        newArray = removeElement(newArray,cell);
                        break;
                    }
                }
                rowArray = rowToArray(game,i);
                colArray = columnToArray(game,j);
                blkArray = blockToArray(game,i,j);
            }

            for (int j = 0; j < 9; j++) {
                System.out.print(" " + game[i][j] + " ");
            }
            System.out.print("\n");
        }*/

        for (int i = 0; i < 9; i++) {
            int[] randomArray = getRandomArray();
            for (int j = 0; j < 9; j++) {
                game[i][j] = randomArray[j];
            }
            for (int j = 0; j < 9; j++) {
                System.out.print(" " + game[i][j] + " ");
            }
            System.out.print("\n");

        }
    }
/*
    //check certain number already exists in an array
    public boolean isValidInArray(int num, int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (num == array[i]) {
                return false;
            }
        }
        return true;
    }

    //pick a random number from array
    public int pickANum(int[] array) {
        return array[(int)(Math.random() * array.length )];
    }

    //transform a row to an array

    public int[] rowToArray(int[][] game, int row) {
        int counter = 0;
        int[] array = new int[9];
        for (int i = 0; i < 9; i++) {
            array[counter] = game[row][i];
            counter++;
        }
        return array;
    }

    //transform a column to an array

    public int[] columnToArray(int[][] game, int col) {
        int counter = 0;
        int[] array = new int[9];
        for (int i = 0; i < 9; i++) {
            array[counter] = game[i][col];
            counter++;
        }
        return array;
    }

    //transform a block to an array

    public int[] blockToArray(int[][] game, int row, int col) {
        int blockRowStart = row / 3 * 3 + 0;
        int blockColStart = col / 3 * 3 + 0;
        int[] array = new int[9];
        int counter = 0;
        for (int i = blockRowStart; i < blockRowStart + 3; i++) {
            for (int j = blockColStart; j < blockColStart + 3; j++) {
                array[counter] = game[i][j];
                counter++;
            }
        }

        return array;
    }*/

    // remove an element from array and return a new array

    public int[] removeElement(int[] array, int num) {
        int[] newArray = new int[array.length - 1];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (num == array[i]) {
                continue;
            }
            else {
                newArray[index] = array[i];
                index++;
            }
        }
        return newArray;
    }

    public int[] getRandomArray() {
        int[] randomArray = new int[9];
        int[] newArray = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int i = 0; newArray.length > 0; i++) {
            randomArray[i] = (int)(Math.random() * (newArray.length - 1 + 1) + 1);
            newArray = removeElement(newArray,randomArray[i]);
        }
        return randomArray;
    }

    public static void main(String[] args) {
        Sudoku newSudoku = new Sudoku();
        newSudoku.start();
    }
}
